﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace FestivalDeMuzik.Models
{
    [Table("IsAmis")]
    public class IsAmis
    {
        [Column("IdIsAmis")]
        public int Id { get; set; }

        [Column("NomFestivalierPrinc")]
        public string NomFestivalierPrinc { get; set; }

        [Column("NomFestivalierSec")]
        public string NomFestivalierSec { get; set; }

        [Column("Etat")]
        public int Etat { get; set; }
    }
}
