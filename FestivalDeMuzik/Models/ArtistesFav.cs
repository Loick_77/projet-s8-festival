﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace FestivalDeMuzik.Models
{
    [Table("ArtistesFav")]
    public class ArtistesFav
    {
        [Column("IdArtistesFav")]
        public int Id { get; set; }

        [Column("NomFestivalier")]
        public string NomFestivalier { get; set; }

        [Column("NomArtiste")]
        public string NomArtiste { get; set; }
    }
}
