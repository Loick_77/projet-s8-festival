﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FestivalDeMuzik.Models
{
    [Table("Artiste")]
    public class Artiste
    {
        [Column("IdArtiste")]
        public int Id { get; set; }
        
        [Column("Nom")]
        [Key]
        public string Nom { get; set; }

        [Column("NomOrganisateur")]
        public string NomOrganisateur { get; set; }

        [Column("PhotoURL")]
        public string PhotoURL { get; set; }

        [Column("Style")]
        public string Style { get; set; }

        [Column("Descriptif")]
        public string Descriptif { get; set; }

        [Column("PaysOrigine")]
        public string PaysOrigine { get; set; }

        [Column("Extrait")]
        public string Extrait { get; set; }
    }




}
