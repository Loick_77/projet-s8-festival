﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FestivalDeMuzik.Models
{
    [Table("Programmation")]
    public class Programmation
    {
        [Column("IdProgrammation")]
        public int Id { get; set; }

        [Column("NomFestival")]
        public string NomFestival { get; set; }

        [Column("NomArtiste")]
        public string NomArtiste { get; set; }

        [Column("NomScene")]
        public string NomScene { get; set; }

        [Column("Heure")]
        public string Heure { get; set; }

        [Column("Lieu")]
        public string Lieu { get; set; }

       
    }
}
