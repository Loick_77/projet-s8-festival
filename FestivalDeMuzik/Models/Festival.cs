﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace FestivalDeMuzik.Models
{
    [Table("Festival")]
    public class Festival
    {
        [Column("IdFestival")]
        public int Id { get; set; }

        [Column("Nom")]
        [Key]
        public string Nom { get; set; }

        [Column("Logo")]
        public string Logo { get; set; }

        [Column("CourtDescriptif")]
        public string CourtDescriptif { get; set; }

        [Column("Commune")]
        public string Commune { get; set; }

        [Column("Departement")]
        public string Departement { get; set; }

        [Column("NomOrganisateur")]
        public string NomOrganisateur { get; set; }

        [Column("NombreDePlaces")]
        [DefaultValue(0)]
        public int NombreDePlaces { get; set; }

        [Column("Date")]
        [DefaultValue(01/01/2000)]
        public DateTime Date { get; set; }

        [Column("Emplacement")]
        public string Emplacement { get; set; }

        [Column("NombreDeVentes")]
        [DefaultValue(0)]
        public int NombreDeVentes { get; set; }

        [Column("IsPublic")]
        [DefaultValue(false)]
        public bool IsPublic { get; set; }

        [Column("PossibiliteDInscription")]
        [DefaultValue(false)]
        public bool PossibiliteDInscription { get; set; }

        [Column("Tarif")]
        [DefaultValue(0)]
        public double Tarif { get; set; }
    }
}
