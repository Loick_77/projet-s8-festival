﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FestivalDeMuzik.Models
{
    [Table("Scene")]
    public class Scene
    {
        [Column("IdScene")]
        public int Id { get; set; }

        [Column("Nom")]
        [Key]
        public string Nom { get; set; }

        [Column("Capacite")]
        public int Capacite { get; set; }

        [Column("Accessibilite")]
        public string Accessibilite { get; set; }
      
      
    }
}
