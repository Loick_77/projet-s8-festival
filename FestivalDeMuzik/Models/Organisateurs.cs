﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FestivalDeMuzik.Models
{
    [Table("Organisateur")]
    public class Organisateur
    {
        [Column("IdOrganisateur")]
        public int Id { get; set; }

        [Column("Login")]
        public string Login{ get; set; }

        [Column("Password")]
        public string Password { get; set; }
        
        [Column("IsGestionnaire")]
        public bool IsGestionnaire { get; set; }
    }
}
