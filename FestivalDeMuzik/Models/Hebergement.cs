﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace FestivalDeMuzik.Models
{
    [Table("Hebergement")]
    public class Hebergement
    {
        [Column("IdHebergement")]
        public int Id { get; set; }

        [Column("Type")]
        public string Type { get; set; }

        [Column("NomFestival")]
        public string NomFestival { get; set; }

        [Column("Nom")]
        public string Nom { get; set; }

        [Column("Lien")]
        public string Lien { get; set; }
    }
}
