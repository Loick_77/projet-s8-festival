﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FestivalDeMuzik.Models
{
    [Table("Festivalier")]
    public class Festivaliers
    {
        [Column("IdFestivalier")]
        public int Id { get; set; }

        [Column("NomFestival")]
        public string NomFestival { get; set; }

        [Column("DateDeNaissance")]
        public DateTime DateDeNaissance { get; set; }

        [Column("DateDInscription")]
        public DateTime DateDInscription { get; set; }

        [Column("Mail")]
        public string Mail { get; set; }

        [Column("Genre")]
        public string Genre { get; set; }

        [Column("Nom")]
        [Key]
        public string Nom { get; set; }

        [Column("Prenom")]
        public string Prenom { get; set; }

        [Column("Password_Festivalier")]
        public string Password_Festivalier { get; set; }
        
        [Column("Telephone")]
        public int Telephone { get; set; }

        [Column("CodePostal")]
        public int CodePostal { get; set; }

        [Column("Commune")]
        public string Commune { get; set; }

        [Column("Pays")]
        public string Pays { get; set; }

        [Column("Formule")]
        public int Formule { get; set; }

        [Column("InscriptionValidee")]
        public bool InscriptionValidee { get; set; }


    }
}
