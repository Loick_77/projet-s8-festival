﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using FestivalDeMuzik.Models;

namespace FestivalDeMuzik.Data
{
    public class FestivalDeMuzikContext7 : DbContext
    {
        public FestivalDeMuzikContext7 (DbContextOptions<FestivalDeMuzikContext7> options)
            : base(options)
        {
        }

        public DbSet<FestivalDeMuzik.Models.Artiste> Artiste { get; set; }

        public DbSet<FestivalDeMuzik.Models.ArtistesFav> ArtistesFav { get; set; }

        public DbSet<FestivalDeMuzik.Models.ArtistesFavSecond> ArtistesFavSecond { get; set; }

        public DbSet<FestivalDeMuzik.Models.Festival> Festival { get; set; }

        public DbSet<FestivalDeMuzik.Models.Festivaliers> Festivaliers { get; set; }

        public DbSet<FestivalDeMuzik.Models.Hebergement> Hebergement { get; set; }

        public DbSet<FestivalDeMuzik.Models.IsAmis> IsAmis { get; set; }

        public DbSet<FestivalDeMuzik.Models.Organisateur> Organisateur { get; set; }

        public DbSet<FestivalDeMuzik.Models.Programmation> Programmation { get; set; }

        public DbSet<FestivalDeMuzik.Models.Scene> Scene { get; set; }
    }
}
