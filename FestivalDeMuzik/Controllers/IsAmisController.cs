﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FestivalDeMuzik.Data;
using FestivalDeMuzik.Models;

namespace FestivalDeMuzik.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IsAmisController : ControllerBase
    {
        private readonly FestivalDeMuzikContext7 _context;

        public IsAmisController(FestivalDeMuzikContext7 context)
        {
            _context = context;
        }

        // GET: api/IsAmis
        [HttpGet]
        public async Task<ActionResult<IEnumerable<IsAmis>>> GetIsAmis()
        {
            return await _context.IsAmis.ToListAsync();
        }

        // GET: api/IsAmis/5
        [HttpGet("{id}")]
        public async Task<ActionResult<IsAmis>> GetIsAmis(int id)
        {
            var isAmis = await _context.IsAmis.FindAsync(id);

            if (isAmis == null)
            {
                return NotFound();
            }

            return isAmis;
        }

        // PUT: api/IsAmis/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutIsAmis(int id, IsAmis isAmis)
        {
            if (id != isAmis.Id)
            {
                return BadRequest();
            }

            _context.Entry(isAmis).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!IsAmisExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/IsAmis
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<IsAmis>> PostIsAmis(IsAmis isAmis)
        {
            _context.IsAmis.Add(isAmis);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetIsAmis", new { id = isAmis.Id }, isAmis);
        }

        // DELETE: api/IsAmis/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<IsAmis>> DeleteIsAmis(int id)
        {
            var isAmis = await _context.IsAmis.FindAsync(id);
            if (isAmis == null)
            {
                return NotFound();
            }

            _context.IsAmis.Remove(isAmis);
            await _context.SaveChangesAsync();

            return isAmis;
        }

        private bool IsAmisExists(int id)
        {
            return _context.IsAmis.Any(e => e.Id == id);
        }
    }
}
