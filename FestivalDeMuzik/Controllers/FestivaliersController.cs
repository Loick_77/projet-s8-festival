﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FestivalDeMuzik.Data;
using FestivalDeMuzik.Models;

namespace FestivalDeMuzik.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FestivaliersController : ControllerBase
    {
        private readonly FestivalDeMuzikContext7 _context;

        public FestivaliersController(FestivalDeMuzikContext7 context)
        {
            _context = context;
        }

        // GET: api/Festivaliers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Festivaliers>>> GetFestivaliers()
        {
            return await _context.Festivaliers.ToListAsync();
        }

        // GET: api/Festivaliers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Festivaliers>> GetFestivaliers(string id)
        {
            var festivaliers = await _context.Festivaliers.FindAsync(id);

            if (festivaliers == null)
            {
                return NotFound();
            }

            return festivaliers;
        }

        // PUT: api/Festivaliers/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFestivaliers(string id, Festivaliers festivaliers)
        {
            if (id != festivaliers.Nom)
            {
                return BadRequest();
            }

            _context.Entry(festivaliers).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FestivaliersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Festivaliers
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Festivaliers>> PostFestivaliers(Festivaliers festivaliers)
        {
            _context.Festivaliers.Add(festivaliers);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (FestivaliersExists(festivaliers.Nom))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetFestivaliers", new { id = festivaliers.Nom }, festivaliers);
        }

        // DELETE: api/Festivaliers/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Festivaliers>> DeleteFestivaliers(string id)
        {
            var festivaliers = await _context.Festivaliers.FindAsync(id);
            if (festivaliers == null)
            {
                return NotFound();
            }

            _context.Festivaliers.Remove(festivaliers);
            await _context.SaveChangesAsync();

            return festivaliers;
        }

        private bool FestivaliersExists(string id)
        {
            return _context.Festivaliers.Any(e => e.Nom == id);
        }
    }
}
