﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FestivalDeMuzik.Data;
using FestivalDeMuzik.Models;

namespace FestivalDeMuzik.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrganisateursController : ControllerBase
    {
        private readonly FestivalDeMuzikContext7 _context;

        public OrganisateursController(FestivalDeMuzikContext7 context)
        {
            _context = context;
        }

        // GET: api/Organisateurs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Organisateur>>> GetOrganisateur()
        {
            return await _context.Organisateur.ToListAsync();
        }

        // GET: api/Organisateurs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Organisateur>> GetOrganisateur(int id)
        {
            var organisateur = await _context.Organisateur.FindAsync(id);

            if (organisateur == null)
            {
                return NotFound();
            }

            return organisateur;
        }

        // GET: api/Organisateurs/77/77
        [HttpGet("{login}/{pwd}")]
        public async Task<ActionResult<Organisateur>> GetOrganisateur(string login, string pwd)
        {
            var organisateur = await _context.Organisateur.FirstOrDefaultAsync(u => u.Login.Equals(login) && u.Password.Equals(pwd) && u.IsGestionnaire == false);

            if (organisateur == null)
            {
                return NotFound();
            }

            return organisateur;
        }

<<<<<<< refs/remotes/origin/master
=======
       
>>>>>>> Ajout session + ouvertureinscriptions etc
        // GET: api/Organisateurs/77/77
        [HttpGet("{login}/{password}/{isGestionnaire}")]
        public async Task<ActionResult<Organisateur>> GetOrganisateur(string login, string password, bool isGestionnaire)
        {
            var organisateur = await _context.Organisateur.FirstOrDefaultAsync(u => u.Login.Equals(login) && u.Password.Equals(password) && u.IsGestionnaire.Equals(isGestionnaire));

            if (organisateur == null)
            {
                return NotFound();
            }

            return organisateur;
        }

<<<<<<< refs/remotes/origin/master


=======
>>>>>>> Ajout session + ouvertureinscriptions etc
        // PUT: api/Organisateurs/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOrganisateur(int id, Organisateur organisateur)
        {
            if (id != organisateur.Id)
            {
                return BadRequest();
            }

            _context.Entry(organisateur).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrganisateurExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Organisateurs
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Organisateur>> PostOrganisateur(Organisateur organisateur)
        {
            _context.Organisateur.Add(organisateur);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetOrganisateur", new { id = organisateur.Id }, organisateur);
        }

        // DELETE: api/Organisateurs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Organisateur>> DeleteOrganisateur(int id)
        {
            var organisateur = await _context.Organisateur.FindAsync(id);
            if (organisateur == null)
            {
                return NotFound();
            }

            _context.Organisateur.Remove(organisateur);
            await _context.SaveChangesAsync();

            return organisateur;
        }

        private bool OrganisateurExists(int id)
        {
            return _context.Organisateur.Any(e => e.Id == id);
        }
    }
}
