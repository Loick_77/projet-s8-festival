﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FestivalDeMuzik.Data;
using FestivalDeMuzik.Models;

namespace FestivalDeMuzik.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArtistesFavsController : ControllerBase
    {
        private readonly FestivalDeMuzikContext7 _context;

        public ArtistesFavsController(FestivalDeMuzikContext7 context)
        {
            _context = context;
        }

        // GET: api/ArtistesFavs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ArtistesFav>>> GetArtistesFav()
        {
            return await _context.ArtistesFav.ToListAsync();
        }

        // GET: api/ArtistesFavs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ArtistesFav>> GetArtistesFav(int id)
        {
            var artistesFav = await _context.ArtistesFav.FindAsync(id);

            if (artistesFav == null)
            {
                return NotFound();
            }

            return artistesFav;
        }

        // PUT: api/ArtistesFavs/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutArtistesFav(int id, ArtistesFav artistesFav)
        {
            if (id != artistesFav.Id)
            {
                return BadRequest();
            }

            _context.Entry(artistesFav).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ArtistesFavExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ArtistesFavs
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ArtistesFav>> PostArtistesFav(ArtistesFav artistesFav)
        {
            _context.ArtistesFav.Add(artistesFav);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetArtistesFav", new { id = artistesFav.Id }, artistesFav);
        }

        // DELETE: api/ArtistesFavs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ArtistesFav>> DeleteArtistesFav(int id)
        {
            var artistesFav = await _context.ArtistesFav.FindAsync(id);
            if (artistesFav == null)
            {
                return NotFound();
            }

            _context.ArtistesFav.Remove(artistesFav);
            await _context.SaveChangesAsync();

            return artistesFav;
        }

        private bool ArtistesFavExists(int id)
        {
            return _context.ArtistesFav.Any(e => e.Id == id);
        }
    }
}
