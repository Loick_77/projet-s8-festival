﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FestivalDeMuzik.Data;
using FestivalDeMuzik.Models;

namespace FestivalDeMuzik.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArtistesFavSecondsController : ControllerBase
    {
        private readonly FestivalDeMuzikContext7 _context;

        public ArtistesFavSecondsController(FestivalDeMuzikContext7 context)
        {
            _context = context;
        }

        // GET: api/ArtistesFavSeconds
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ArtistesFavSecond>>> GetArtistesFavSecond()
        {
            return await _context.ArtistesFavSecond.ToListAsync();
        }

        // GET: api/ArtistesFavSeconds/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ArtistesFavSecond>> GetArtistesFavSecond(int id)
        {
            var artistesFavSecond = await _context.ArtistesFavSecond.FindAsync(id);

            if (artistesFavSecond == null)
            {
                return NotFound();
            }

            return artistesFavSecond;
        }

        // PUT: api/ArtistesFavSeconds/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutArtistesFavSecond(int id, ArtistesFavSecond artistesFavSecond)
        {
            if (id != artistesFavSecond.Id)
            {
                return BadRequest();
            }

            _context.Entry(artistesFavSecond).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ArtistesFavSecondExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ArtistesFavSeconds
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ArtistesFavSecond>> PostArtistesFavSecond(ArtistesFavSecond artistesFavSecond)
        {
            _context.ArtistesFavSecond.Add(artistesFavSecond);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetArtistesFavSecond", new { id = artistesFavSecond.Id }, artistesFavSecond);
        }

        // DELETE: api/ArtistesFavSeconds/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ArtistesFavSecond>> DeleteArtistesFavSecond(int id)
        {
            var artistesFavSecond = await _context.ArtistesFavSecond.FindAsync(id);
            if (artistesFavSecond == null)
            {
                return NotFound();
            }

            _context.ArtistesFavSecond.Remove(artistesFavSecond);
            await _context.SaveChangesAsync();

            return artistesFavSecond;
        }

        private bool ArtistesFavSecondExists(int id)
        {
            return _context.ArtistesFavSecond.Any(e => e.Id == id);
        }
    }
}
