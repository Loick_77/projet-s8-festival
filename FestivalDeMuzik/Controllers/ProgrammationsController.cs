﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FestivalDeMuzik.Data;
using FestivalDeMuzik.Models;

namespace FestivalDeMuzik.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProgrammationsController : ControllerBase
    {
        private readonly FestivalDeMuzikContext7 _context;

        public ProgrammationsController(FestivalDeMuzikContext7 context)
        {
            _context = context;
        }

        // GET: api/Programmations
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Programmation>>> GetProgrammation()
        {
            return await _context.Programmation.ToListAsync();
        }

        // GET: api/Programmations/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Programmation>> GetProgrammation(int id)
        {
            var programmation = await _context.Programmation.FindAsync(id);

            if (programmation == null)
            {
                return NotFound();
            }

            return programmation;
        }

        // PUT: api/Programmations/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProgrammation(int id, Programmation programmation)
        {
            if (id != programmation.Id)
            {
                return BadRequest();
            }

            _context.Entry(programmation).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgrammationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Programmations
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Programmation>> PostProgrammation(Programmation programmation)
        {
            _context.Programmation.Add(programmation);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProgrammation", new { id = programmation.Id }, programmation);
        }

        // DELETE: api/Programmations/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Programmation>> DeleteProgrammation(int id)
        {
            var programmation = await _context.Programmation.FindAsync(id);
            if (programmation == null)
            {
                return NotFound();
            }

            _context.Programmation.Remove(programmation);
            await _context.SaveChangesAsync();

            return programmation;
        }

        private bool ProgrammationExists(int id)
        {
            return _context.Programmation.Any(e => e.Id == id);
        }
    }
}
