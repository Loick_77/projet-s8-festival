﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FestivalDeMuzik.Data;
using FestivalDeMuzik.Models;

namespace FestivalDeMuzik.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FestivalsController : ControllerBase
    {
        private readonly FestivalDeMuzikContext7 _context;

        public FestivalsController(FestivalDeMuzikContext7 context)
        {
            _context = context;
        }

        // GET: api/Festivals
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Festival>>> GetFestival()
        {
            return await _context.Festival.ToListAsync();
        }

        // GET: api/Festivals/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Festival>> GetFestival(string id)
        {
            var festival = await _context.Festival.FindAsync(id);

            if (festival == null)
            {
                return NotFound();
            }

            return festival;
        }


        /*// GET: api/Festivals/77/77
        [HttpGet("{nomOrganisateur}")]
        public async Task<ActionResult<Festival>> GetFestival(string nomOrganisateur)
        {
            var festival = await _context.Festival.FirstOrDefaultAsync(u => u.NomOrganisateur.Equals(nomOrganisateur));

            if (festival == null)
            {
                return NotFound();
            }

            return festival;
        }*/

        // PUT: api/Festivals/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFestival(string id, Festival festival)
        {
            if (id != festival.Nom)
            {
                return BadRequest();
            }

            _context.Entry(festival).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FestivalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Festivals
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Festival>> PostFestival(Festival festival)
        {
            _context.Festival.Add(festival);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (FestivalExists(festival.Nom))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetFestival", new { id = festival.Nom }, festival);
        }

        // DELETE: api/Festivals/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Festival>> DeleteFestival(string id)
        {
            var festival = await _context.Festival.FindAsync(id);
            if (festival == null)
            {
                return NotFound();
            }

            _context.Festival.Remove(festival);
            await _context.SaveChangesAsync();

            return festival;
        }

        private bool FestivalExists(string id)
        {
            return _context.Festival.Any(e => e.Nom == id);
        }
    }
}
