﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientLourdGroupe4
{
    /// <summary>
    /// Logique d'interaction pour Home.xaml
    /// </summary>
    public partial class Home : Page
    {
        public Home()
        {
            InitializeComponent();
        }
        private void Button_Click_artiste(object sender, RoutedEventArgs e)
        {
            // View Expense Report
            AjoutArtiste ajoutArtiste = new AjoutArtiste();
            this.NavigationService.Navigate(ajoutArtiste);
        }
        private void Button_Click_gestionFestival(object sender, RoutedEventArgs e)
        {
            // View Expense Report
            GestionFestival gestionFestival = new GestionFestival();
            this.NavigationService.Navigate(gestionFestival);
        }
        private void Button_Click_publiFestival(object sender, RoutedEventArgs e)
        {
            // View Expense Report
            PubliFestival publiFestival = new PubliFestival();
            this.NavigationService.Navigate(publiFestival);
        }
    }
}
