﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ClientLourdGroupe4.ControllersAPI;
using FestivalDeMuzik.Models;

namespace ClientLourdGroupe4
{
    /// <summary>
    /// Logique d'interaction pour FormulaireAjoutFestival.xaml
    /// </summary>
    public partial class FormulaireModifFestival : Page
    {
        public FormulaireModifFestival()
        {
            InitializeComponent();
        }
    
    private void ValidationFormModif(object sender, RoutedEventArgs e)
    {
            Festival festival = new Festival
            {
                Nom = Nomtb.Text,
                Logo = Logotb.Text,
                CourtDescriptif = CourtDescriptiftb.Text,
                Commune = Communetb.Text,
                Departement = Departementtb.Text
            };
            _ = API.Instance.ModifFestivalAsync(festival);
            Nomtb.Clear();
            Departementtb.Clear();
            Communetb.Clear();
            CourtDescriptiftb.Clear();
            Logotb.Clear();
            MessageBox.Show("Festival modifé !", "Enregistrement effectué", MessageBoxButton.OK, MessageBoxImage.Information);
            FormulaireModifFestival formulaireModifFestival = new FormulaireModifFestival();
        }
    }
}
