﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientLourdGroupe4
{
    /// <summary>
    /// Logique d'interaction pour GestionFestival.xaml
    /// </summary>
    public partial class GestionFestival : Page
    {
        public GestionFestival()
        {
            InitializeComponent();
        }
        private void Button_Click_gestionFestival(object sender, RoutedEventArgs e)
        {
            // View Expense Report
            GestionFestival gestionFestival = new GestionFestival();
            this.NavigationService.Navigate(gestionFestival);
        }
        private void Button_Click_ajoutFestival(object sender, RoutedEventArgs e)
        {
            // View Expense Report
            FormulaireAjoutFestival formulaireAjoutFestival = new FormulaireAjoutFestival();
            this.NavigationService.Navigate(formulaireAjoutFestival);
        }
        private void Button_Click_modifFestival(object sender, RoutedEventArgs e)
        {
            // View Expense Report
            FormulaireModifFestival formulaireModifFestival = new FormulaireModifFestival();
            this.NavigationService.Navigate(formulaireModifFestival);
        }
    }
}
