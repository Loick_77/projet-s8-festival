﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ClientLourdGroupe4.ControllersAPI;
using FestivalDeMuzik.Models;

namespace ClientLourdGroupe4
{
    /// <summary>
    /// Logique d'interaction pour AjoutArtiste.xaml
    /// </summary>
    public partial class AjoutArtiste : Page
    {
        public AjoutArtiste()
        {
            InitializeComponent();
        }

        private void BtAjoutArtiste_Click(object sender, RoutedEventArgs e)
        {
            if (NomTb.Text.Length > 0)
            {
                if (StyleTb.Text.Length > 0)
                {
                    if (PaysOrigineTb.Text.Length > 0)
                    {

                        if (DescriptionTb.Text.Length > 0)
                        {

                            Artiste artiste = new Artiste
                            {
                                Nom = NomTb.Text,
                                Style = StyleTb.Text,
                                PaysOrigine = PaysOrigineTb.Text,
                                Descriptif = DescriptionTb.Text,
                                PhotoURL = "None",
                                Extrait = "None",
                            };
                            _ = API.Instance.AjoutArtisteAsync(artiste);
                            NomTb.Clear();
                            StyleTb.Clear();
                            PaysOrigineTb.Clear();
                            DescriptionTb.Clear();


                            MessageBox.Show("Artiste ajouté !", "Enregistrement effectué", MessageBoxButton.OK, MessageBoxImage.Information);
                            AjoutArtiste ajoutartiste = new AjoutArtiste();
                            
                        }
                        else
                        {
                            MessageBox.Show("Le descriptif doit être complété !", "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                        }

                    }
                    else
                    {
                        MessageBox.Show("Le pays d'origine doit être renseigné !", "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Le style doit être renseigné !", "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                }

            }
            else
            {
                MessageBox.Show("Le nom de l'artiste  doit être renseigné !", "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
