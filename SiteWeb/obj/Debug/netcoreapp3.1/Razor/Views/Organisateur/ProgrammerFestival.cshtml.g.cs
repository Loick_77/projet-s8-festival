#pragma checksum "C:\Users\jourd\Source\Repos\projet-s8-festival\SiteWeb\Views\Organisateur\ProgrammerFestival.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "27a134361506621bd0167c637c1f15c48b5965a6"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Organisateur_ProgrammerFestival), @"mvc.1.0.view", @"/Views/Organisateur/ProgrammerFestival.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\jourd\Source\Repos\projet-s8-festival\SiteWeb\Views\_ViewImports.cshtml"
using SiteWeb;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\jourd\Source\Repos\projet-s8-festival\SiteWeb\Views\_ViewImports.cshtml"
using SiteWeb.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"27a134361506621bd0167c637c1f15c48b5965a6", @"/Views/Organisateur/ProgrammerFestival.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"850658dcf7b51214584ba033e6bdc9bbc6f07b4a", @"/Views/_ViewImports.cshtml")]
    public class Views_Organisateur_ProgrammerFestival : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 2 "C:\Users\jourd\Source\Repos\projet-s8-festival\SiteWeb\Views\Organisateur\ProgrammerFestival.cshtml"
  
    ViewData["Title"] = "ProgrammerFestival";
    Layout = "~/Views/Shared/ViewForOrganiser.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h2>ProgrammerFestival</h2>\r\n\r\n<h3><strong>");
#nullable restore
#line 9 "C:\Users\jourd\Source\Repos\projet-s8-festival\SiteWeb\Views\Organisateur\ProgrammerFestival.cshtml"
       Write(ViewData["NomFestival"]);

#line default
#line hidden
#nullable disable
            WriteLiteral(" ");
#nullable restore
#line 9 "C:\Users\jourd\Source\Repos\projet-s8-festival\SiteWeb\Views\Organisateur\ProgrammerFestival.cshtml"
                                Write(ViewBag.MessageErreur);

#line default
#line hidden
#nullable disable
            WriteLiteral(" ");
#nullable restore
#line 9 "C:\Users\jourd\Source\Repos\projet-s8-festival\SiteWeb\Views\Organisateur\ProgrammerFestival.cshtml"
                                                       Write(ViewData["NomOrga"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("</strong></h3>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
