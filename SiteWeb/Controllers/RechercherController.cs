﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using FestivalDeMuzik.Models;
using SiteWeb.Models;
using Microsoft.EntityFrameworkCore;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SiteWeb.Controllers
{
    public class RechercherController : Controller
    {
        private readonly FestivalDeMuzik.Data.FestivalDeMuzikContext7 _context;

        public RechercherController(FestivalDeMuzik.Data.FestivalDeMuzikContext7 context)
        {
            _context = context;
        }

        // GET: /<controller>/
        public async Task<IActionResult> Rechercher(string SearchString)
        {

            var artistes = from a in _context.Artiste
                           select a;
            if (!string.IsNullOrEmpty(SearchString))
            {
                artistes = artistes.Where(s => s.Nom.Contains(SearchString));
            }

            return View(await artistes.ToListAsync());
        }
    }
}
