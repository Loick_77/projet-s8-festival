﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using FestivalDeMuzik.Models;
using SiteWeb.Models;
using Microsoft.EntityFrameworkCore;

namespace SiteWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

    
      


        public IActionResult Index()
        {
            ICollection<Artiste> artistes = APIControl.Instance.GetArtiste().Result;
            ViewData["artistes"] = artistes;
            return View();
        }

        public IActionResult ConsulterInfosGenerales()
        {
            ICollection<Festival> festivals = APIControl.Instance.GetFest().Result;
            ViewData["festivals"] = festivals;
            return View();
        }

        
        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult ProgrammationFestival()
        {
            ICollection<Programmation> programmations = APIControl.Instance.GetProgramme().Result ;
            ViewData["programmations"] = programmations;
            ICollection<Scene> scenes = APIControl.Instance.GetScene().Result;
            ViewData["scenes"] = scenes;
            return View();
        }

      


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
