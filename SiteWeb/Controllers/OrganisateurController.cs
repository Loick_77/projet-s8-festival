﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using FestivalDeMuzik.Models;
using SiteWeb.Models;
using Microsoft.EntityFrameworkCore;
<<<<<<< refs/remotes/origin/master
using ClientLourdGroupe4.ControllersAPI;
=======
using Microsoft.AspNetCore.Http;
>>>>>>> Ajout session + ouvertureinscriptions etc

namespace SiteWeb.Controllers
{
    public class OrganisateurController : Controller
    {
        public IActionResult InscriptionOrganisateur()
        {
            Organisateur organisateur = new Organisateur();
            if (Request.Method == "POST")
            {
                string recuLogin = Request.Form["Login"];
                string recuMdp = Request.Form["Password"];
                organisateur.Login = recuLogin;
                organisateur.Password = recuMdp;
                organisateur.IsGestionnaire = false;
                Uri url = APIControl.Instance.AjoutOrganisateurAsync(organisateur).Result;

                if (url == null)
                {
                    return View("Views/Organisateur/InscriptionOrganisateur.cshtml");
                }
                else
                {
                    Organisateur organisateur1 = APIControl.Instance.AuthOrg(recuLogin, recuMdp).Result;
                    ViewData["organisateur"] = organisateur1;
                    return View("Views/Organisateur/ConnexionOrganisateur.cshtml");
                }
            }
            else { return View("Views/Organisateur/InscriptionOrganisateur.cshtml"); }
        }

        public IActionResult ConnexionOrganisateur()
        {
            OrganisateurViewModel viewModel = new OrganisateurViewModel { Authentifie = false };
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult ConnexionOrganisateur(OrganisateurViewModel viewModel)
        {
            if (Request.Method == "POST")
            {
                string recuLogin = Request.Form["Login"];
                string recuMdp = Request.Form["Password"];
                Organisateur organisateur = APIControl.Instance.AuthOrg(recuLogin, recuMdp).Result;
                if (organisateur == null)
                {
                    return View("Views/Home/Index.cshtml", viewModel);
                }
                else
                {
                    ViewData["organisateur"] = organisateur;

                    HttpContext.Session.SetInt32("identifiant", organisateur.Id);
                    TempData["IdOrganisateur"] = organisateur.Id;
                    TempData.Keep();

                    return Redirect("Index");
                }
            }
            else
            {
                return View();
            }
        }

        public IActionResult ConfirmationInscription()
        {
            return View();
        }

        public ActionResult Deconnexion(OrganisateurViewModel viewModel)
        {
<<<<<<< refs/remotes/origin/master
            return View("../Home/Index");
=======
            HttpContext.Session.Clear();
            return View("Views/Home/Index.cshtml");
>>>>>>> Ajout session + ouvertureinscriptions etc
        }
        public IActionResult Index()
        {
            int? id = HttpContext.Session.GetInt32("identifiant");
            Organisateur orga = APIControl.Instance.GetOrganisateur(id).Result;
            if (orga == null)
            {
                ViewBag.Message = "Utilisateur non trouvé";
            }
            else
            {
                TempData["NomOrganisateur"] = orga.Login;
            }
            return View();
        }
        public ActionResult OuverturesInscriptions()
        {
            ICollection<Festival> festivals = APIControl.Instance.GetFest().Result;
            if (Request.Method == "POST")
            {
                int festid = Int32.Parse(Request.Form["idname"]);
                bool ispublic = Convert.ToBoolean(Request.Form["gestioninsc"]);
                Festival festival = new Festival { };
                festival = APIControl.Instance.GetFestival(festid).Result;
                festival.PossibiliteDInscription = ispublic;
<<<<<<< refs/remotes/origin/master
                _ = API.Instance.ModifFestivalAsync(festival);
=======
                _ = APIControl.Instance.ModifFestivalAsync(festival);
>>>>>>> Ajout session + ouvertureinscriptions etc
                ViewData["festivals"] = festivals;
                return View();
            }
            else
            {
                ViewData["festivals"] = festivals;
                return View();
            }
<<<<<<< refs/remotes/origin/master
=======
        }

        public IActionResult ProgrammerFestival()
        {
            int? id = HttpContext.Session.GetInt32("identifiant");
            Organisateur orga = APIControl.Instance.GetOrganisateur(id).Result;
            string nomOrganisateur = orga.Login;
            Festival fest = APIControl.Instance.GetOrgaFest(nomOrganisateur).Result;
            if (fest == null)
            {
                ViewBag.MessageErreur = "Aucun festival ne vous a encore été attribué!";
                ViewData["NomOrga"] = nomOrganisateur;
            }
            else
            {
                ViewData["NomFestival"] = fest.Nom;
            }
            
            return View();
>>>>>>> Ajout session + ouvertureinscriptions etc
        }
        
    }
}