﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using FestivalDeMuzik.Models;
using SiteWeb.Models;
using Microsoft.EntityFrameworkCore;


namespace SiteWeb.Controllers
{
    public class ArtisteController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult EnregistrementArtiste()
        {
            Artiste artiste = new Artiste();
            if (Request.Method == "POST")
            {
                string recuNom = Request.Form["Nom"];
                string recuPhotoURL = Request.Form["PhotoURL"];
                string recuStyle = Request.Form["Style"];
                string recuCourtDescriptif = Request.Form["Descriptif"];
                string recuPaysOrigine = Request.Form["PaysOrigine"];
                string recuExtrait = Request.Form["Extrait"];
                artiste.Nom = recuNom;
                artiste.PhotoURL = recuPhotoURL;
                artiste.Style = recuStyle;
                artiste.Descriptif = recuCourtDescriptif;
                artiste.PaysOrigine = recuPaysOrigine;
                artiste.Extrait = recuExtrait;
                Uri url = APIControl.Instance.AjoutArtisteAsync(artiste).Result;

                if (url == null)
                {
                    return View("Views/Artiste/EnregistrementArtiste.cshtml");
                }
                else
                {
                    return View("Views/Organisateur/Index.cshtml");
                }
            }
            else { return View("Views/Artiste/EnregistrementArtiste.cshtml"); }
        }

        [HttpPost]
        public ActionResult AjoutArtiste(string artiste)
        {
            ArtistesFav favorite = new ArtistesFav();
            //favorite.NomFestivalier = Session["nom"].ToString();
            favorite.NomArtiste = artiste;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:56544/api/");

                var postTask = client.PostAsJsonAsync<ArtistesFav>("ArtistesFav", favorite);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    ViewBag.Message = "Nouvel artiste ajouté";
                }
                else
                {
                    ViewBag.Message = "Erreur";
                }
            }
            return RedirectToAction("../Artiste/AjoutSelecPrincArtistesFav");
        }
    }
}