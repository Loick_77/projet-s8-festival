﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using FestivalDeMuzik.Models;
using FestivalDeMuzik.Controllers;
using FestivalDeMuzik.Data;
using Newtonsoft.Json;
using SiteWeb.Models;
using Microsoft.EntityFrameworkCore;

namespace SiteWeb.Controllers
{
    public class APIControl
    {
        HttpClient client = new HttpClient();
        public APIControl()
        {
            client.BaseAddress = new Uri("http://localhost:56544/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        private static readonly object padlock = new object();
        private static APIControl instance = null;
      

        public static APIControl Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new APIControl();
                    }
                    return instance;
                }
            }
        }

        public async Task<Festival> GetFestival()
        {
            Festival fest = null;
            HttpResponseMessage response = client.GetAsync("api/Festivals/").Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                fest = JsonConvert.DeserializeObject<Festival>(resp);
            }
            return fest;
        }

        public async Task<Festival> GetFestival(int id)
        {
            Festival fest = null;
            HttpResponseMessage response = client.GetAsync("api/Festivals/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                fest = JsonConvert.DeserializeObject<Festival>(resp);
            }
            return fest;
        }

<<<<<<< refs/remotes/origin/master
=======
        public async Task<Uri> ModifFestivalAsync(Festival festival)
        {
            try
            {
                HttpResponseMessage response = await client.PutAsJsonAsync("api/festivals/" + festival.Id, festival);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }


        //GET un organisateur: sert pour la session
        public async Task<Organisateur> GetOrganisateur(int? id)
        {
            Organisateur orga = null;
            HttpResponseMessage response = client.GetAsync("api/Organisateurs/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                orga = JsonConvert.DeserializeObject<Organisateur>(resp);
            }

            return orga;
        }
>>>>>>> Ajout session + ouvertureinscriptions etc

        public async Task<Uri> AjoutOrganisateurAsync(Organisateur organisateur)
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("api/Organisateurs", organisateur);
                response.EnsureSuccessStatusCode();
                Console.WriteLine($"Créé à {response.Headers.Location}");
                return response.Headers.Location;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<Uri> AjoutArtisteAsync(Artiste artiste)
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("api/Artistes", artiste);
                response.EnsureSuccessStatusCode();
                Console.WriteLine($"Créé à {response.Headers.Location}");
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<Organisateur> AuthOrg(string login, string password)
        {
            Organisateur org = null;
            HttpResponseMessage response = client.GetAsync("api/Organisateurs/" + login + "/" + password + "/" + false).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                org = JsonConvert.DeserializeObject<Organisateur>(resp);
            }

            return org;
        }



        //GET : Festival en fonction de nom organisateur
        public async Task<Festival> GetOrgaFest(string nomOrga)
        {
            Festival fest = null;
            HttpResponseMessage response = client.GetAsync("api/Festivals/" + nomOrga).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                fest = JsonConvert.DeserializeObject<Festival>(resp);
            }

            return fest;
        }

        public async Task<ICollection<Festival>> GetFest()
        {
            ICollection<Festival> ListeFestivals = new List<Festival>();
            HttpResponseMessage response = client.GetAsync("api/Festivals").Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                ListeFestivals = JsonConvert.DeserializeObject<List<Festival>>(resp);
            }

            return ListeFestivals;
        }

        public async Task<ICollection<Artiste>> GetArtiste()
        {
            ICollection<Artiste> ListeArtistes = new List<Artiste>();
            HttpResponseMessage response = client.GetAsync("api/Artistes").Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                ListeArtistes = JsonConvert.DeserializeObject<List<Artiste>>(resp);
            }

            return ListeArtistes;
        }

        public async Task<ICollection<Programmation>> GetProgramme()
        {
            ICollection<Programmation> ListeProgrammes = new List<Programmation>();
            HttpResponseMessage response = client.GetAsync("api/Programmations").Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                ListeProgrammes = JsonConvert.DeserializeObject<List<Programmation>>(resp);
            }

            return ListeProgrammes;
        }
        public async Task<ICollection<Scene>> GetScene()
        {
            ICollection<Scene> ListeScenes = new List<Scene>();
            HttpResponseMessage response = client.GetAsync("api/Scenes").Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                ListeScenes = JsonConvert.DeserializeObject<List<Scene>>(resp);
            }

            return ListeScenes;
        }

    }
}
