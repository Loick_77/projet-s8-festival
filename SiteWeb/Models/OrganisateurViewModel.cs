﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FestivalDeMuzik.Models;

namespace SiteWeb.Models
{
    public class OrganisateurViewModel
    {
        public Organisateur Organisateur { get; set; }
        public bool Authentifie { get; set; }
    }
}
