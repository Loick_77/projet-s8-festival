﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FestivalSiteWeb.Models
{
    public class Festivaliers
    {
        public int Id { get; set; }

        public string NomFestival { get; set; }

        public DateTime DateDeNaissance { get; set; }

        public DateTime DateDInscription { get; set; }

        public string Mail { get; set; }

        public string Genre { get; set; }

        [Key]
        public string Nom { get; set; }

        public string Prenom { get; set; }

        public string Password_Festivalier { get; set; }

        public int Telephone { get; set; }

        public int CodePostal { get; set; }

        public string Commune { get; set; }

        public string Pays { get; set; }

        public int Formule { get; set; }

        public bool InscriptionValidee { get; set; }
    }
}