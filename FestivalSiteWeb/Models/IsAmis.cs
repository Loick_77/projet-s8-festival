﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FestivalSiteWeb.Models
{
    public class IsAmis
    {
        public int Id { get; set; }

        public string NomFestivalierPrinc { get; set; }

        public string NomFestivalierSec { get; set; }

        public int Etat { get; set; }
    }
}