﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FestivalSiteWeb.Models
{
    public class Artiste
    {
        public int Id { get; set; }

        [Key]
        public string Nom { get; set; }

        public string NomOrganisateur { get; set; }

        public string PhotoURL { get; set; }

        public string Style { get; set; }

        public string Descriptif { get; set; }

        public string PaysOrigine { get; set; }

        public string Extrait { get; set; }
    }
}