﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FestivalSiteWeb.Models
{
    public class Hebergement
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public string NomFestival { get; set; }

        public string Nom { get; set; }

        public string Lien { get; set; }
    }
}