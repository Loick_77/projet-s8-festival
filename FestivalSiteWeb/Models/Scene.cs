﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FestivalSiteWeb.Models
{
    public class Scene
    {
        public int Id { get; set; }

        [Key]
        public string Nom { get; set; }

        public int Capacite { get; set; }

        public string Accessibilite { get; set; }
    }
}