﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FestivalSiteWeb.Models
{
    public class ArtistesFavSecond
    {
        public int Id { get; set; }
        public string NomFestivalier { get; set; }
        public string NomArtiste { get; set; }
    }
}