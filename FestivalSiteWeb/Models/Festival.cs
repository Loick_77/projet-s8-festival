﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FestivalSiteWeb.Models
{
    public class Festival
    {
        public int Id { get; set; }

        [Key]
        public string Nom { get; set; }

        public string Logo { get; set; }

        public string CourtDescriptif { get; set; }

        public string Commune { get; set; }

        public string Departement { get; set; }

        public string NomOrganisateur { get; set; }

        public int NombreDePlaces { get; set; }

        public DateTime Date { get; set; }

        public string Emplacement { get; set; }

        public int NombreDeVentes { get; set; }

        public bool IsPublic { get; set; }
        
        public bool PossibiliteDInscription { get; set; }

        public double Tarif { get; set; }
    }
}