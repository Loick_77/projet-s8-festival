﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FestivalSiteWeb.Models
{
    public class Programmation
    {
        public int Id { get; set; }

        public string NomFestival { get; set; }

        public string NomArtiste { get; set; }

        public string NomScene { get; set; }

        public string Heure { get; set; }

        public string Lieu { get; set; }

    }
}