﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using FestivalSiteWeb.Models;

namespace FestivalSiteWeb.Controllers
{
    public class ArtisteController : Controller
    {
        // GET: Artiste
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult EnregistrementArtiste()
        {
            return View();
        }
        [HttpPost]
        public ActionResult EnregistrementArtiste(FormCollection collection)
        {
            Artiste artiste = new Artiste();
           
                string recuNom = Request.Form["Nom"];
                string recuPhotoURL = Request.Form["PhotoURL"];
                string recuStyle = Request.Form["Style"];
                string recuCourtDescriptif = Request.Form["Descriptif"];
                string recuPaysOrigine = Request.Form["PaysOrigine"];
                string recuExtrait = Request.Form["Extrait"];
                artiste.Nom = recuNom;
                artiste.PhotoURL = recuPhotoURL;
                artiste.Style = recuStyle;
                artiste.Descriptif = recuCourtDescriptif;
                artiste.PaysOrigine = recuPaysOrigine;
                artiste.Extrait = recuExtrait;
                Uri url = APIControl.Instance.AjoutArtisteAsync(artiste).Result;

                if (url == null)
                {
                    return View("../Artiste/EnregistrementArtiste");
                }
                else
                {
                    return View("../Organisateur/Index");
                }
           
        }

        [HttpPost]
        public ActionResult AjoutArtiste(string artiste)
        {
            ArtistesFav favorite = new ArtistesFav();
            favorite.NomFestivalier = Session["nom"].ToString();
            favorite.NomArtiste = artiste;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:56544/api/");

                var postTask = client.PostAsJsonAsync<ArtistesFav>("ArtistesFav", favorite);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    ViewBag.Message = "Nouvel artiste ajouté";
                }
                else
                {
                    ViewBag.Message = "Erreur";
                }
            }
            return RedirectToAction("../Artiste/AjoutSelecPrincArtistesFav");
        }
    }
}