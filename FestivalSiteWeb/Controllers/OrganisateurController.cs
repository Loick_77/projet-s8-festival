﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using FestivalSiteWeb.Models;

namespace FestivalSiteWeb.Controllers
{
    public class OrganisateurController : Controller
    {
        // GET: Organisateur
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult InscriptionOrganisateur()
        {
            return View();
        }

        // POST: Participant/ Create
        [HttpPost]

        public ActionResult InscriptionOrganisateur(FormCollection collection)
        {
            Organisateur organisateur = new Organisateur();
           
                string recuLogin = collection["Login"];
                string recuMdp = collection["Password"];
                organisateur.Login = recuLogin;
                organisateur.Password = recuMdp;
                organisateur.IsGestionnaire = false;
                Uri url = APIControl.Instance.AjoutOrganisateurAsync(organisateur).Result;

                if (url == null)
                {
                    return View("../Organisateur/InscriptionOrganisateur");
                }
                else
                {
                    Organisateur organisateur1 = APIControl.Instance.AuthOrg(recuLogin, recuMdp).Result;
                    ViewData["organisateur"] = organisateur1;
                    return View("../Organisateur/ConnexionOrganisateur");
                }
           
        }


        public ActionResult ConnexionOrganisateur()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ConnexionOrganisateur(FormCollection collection)
        {
           
                string recuLogin = Request.Form["Login"];
                string recuMdp = Request.Form["Password"];
                Organisateur organisateur = APIControl.Instance.AuthOrg(recuLogin, recuMdp).Result;
                if (organisateur == null)
                {
                    return View("../Home/Index");
                }
                else
                {
                    ViewData["organisateur"] = organisateur;
                    return View("../Organisateur/Index");
                }
           
        }
        
       
        /*
        [HttpPost]
        public ActionResult ConnexionOrganisateur(FormCollection collection)
        {
            IEnumerable<Organisateur> organisateurs = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:56544/api/");

                var responseTask = client.GetAsync("Organisers");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<Organisateur>>();
                    readTask.Wait();
                    organisateurs = readTask.Result;

                    foreach (var org in organisateurs)
                    {
                        if (collection["Login"] == org.Login && collection["Password"] == org.Password)
                        {
                            Session["Login"] = collection["Login"];
                        }
                    }

                    return View("../Organisateur/Index");
                }
                else
                {
                    organisateurs = Enumerable.Empty<Organisateur>();

                    ModelState.AddModelError(string.Empty, "Erreur du serveur. Contactez l'administrateur s'il vous plaît.");
                }
                return RedirectToAction("../Organisateur/ConnexionOrganisateur");
            }
        }*/

        public ActionResult ConfirmationInscription()
        {
            return View();
        }

        public ActionResult Deconnexion()
        {
            return View("../Home/Index");
        }
      
        public ActionResult OuverturesInscriptions()
        {
            ICollection<Festival> festivals = APIControl.Instance.GetFest().Result;
            ViewData["festivals"] = festivals;
            return View();
        }
    }
}