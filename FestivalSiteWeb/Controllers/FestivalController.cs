﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using FestivalSiteWeb.Models;

namespace FestivalSiteWeb.Controllers
{
    public class FestivalController : Controller
    {
        // GET: Festival
        public ActionResult Index()
        {
            IEnumerable<Festival> Festivals = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:56544/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Festivals");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<Festival>>();
                    readTask.Wait();

                    Festivals = readTask.Result;
                }
                else 
                {

                    Festivals = Enumerable.Empty<Festival>();

                    ModelState.AddModelError(string.Empty, "Erreur du serveur. Contactez l'administrateur s'il vous plaît.");
                }
            }
            return View(Festivals);
        }

    }
}