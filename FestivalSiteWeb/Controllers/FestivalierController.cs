﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using FestivalSiteWeb.Models;

namespace FestivalSiteWeb.Controllers
{
    public class FestivalierController : Controller
    {
        // GET: Festivalier
        public ActionResult Index(string nom)
        {
            ViewBag.nom = nom;
            return View();
        }
        public ActionResult InscriptionFestivalier(string NomFestival)
        {
            NomFestival = Request.QueryString["fname"];
            TempData["NomFestival"] = NomFestival;
            return View();
        }

        // POST: Participant/ Create
        [HttpPost]
        public ActionResult InscriptionFestivalier(FormCollection collection)
        {
            Festivaliers f = new Festivaliers();
            f.Nom = collection["Nom"];
            f.Prenom = collection["Prenom"];
            if (TempData.ContainsKey("NomFestival"))
                f.NomFestival = TempData["NomFestival"].ToString();
            f.DateDInscription = DateTime.Now;
            f.DateDeNaissance = Convert.ToDateTime(collection["Date"]);
            f.Genre = collection["Genre"];
            f.CodePostal = Convert.ToInt32(collection["CodePostal"]);
            f.Commune = collection["Commune"];
            f.Pays = collection["Pays"];
            f.Password_Festivalier = collection["Password_Festivalier"];
            f.InscriptionValidee= true;
            f.Mail = collection["Email"];

            TempData["FestiName"] = f.NomFestival;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:56544/api/");

                var postTask = client.PostAsJsonAsync<Festivaliers>("Festivaliers", f);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var respTask = client.GetAsync("Festivals?Nom=" + f.NomFestival);
                    respTask.Wait();

                    var res = respTask.Result;
                    if (res.IsSuccessStatusCode)
                    {
                        var rdTask = res.Content.ReadAsAsync<Festival>();
                        rdTask.Wait();

                        Festival fe = rdTask.Result;

                        fe.NombreDeVentes = fe.NombreDeVentes - 1;

                        var putTask = client.PutAsJsonAsync<Festival>("Festivals", fe);
                        putTask.Wait();

                        var results = putTask.Result;
                    }
                    Session["NnomFestival"] = f.NomFestival;
                    Session["nom"] = f.Nom;
                    return RedirectToAction("../Festivalier/Index", new { nom = f.Nom });
                }
                else
                {
                    Response.Write(f.NomFestival);
                    return View("../Festivalier/InscriptionFestivalier");
                }
            }

        }


        public ActionResult Connexion(string FName)
        {
            FName = Request.QueryString["fname"];
            TempData["FName"] = FName;
            return View();
        }

        // POST: Participant/ Login
        [HttpPost]
        public ActionResult Connexion(FormCollection collection)
        {
            IEnumerable<Festivaliers> festivaliers = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:56544/api/");

                var responseTask = client.GetAsync("Festivaliers");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<Festivaliers>>();
                    readTask.Wait();
                    festivaliers = readTask.Result;

                    foreach (var f in festivaliers)
                    {
                        if (f.Nom == collection["inputName"] && f.Password_Festivalier == collection["inputPassword"])
                        {
                            Session["nomFestival"] = TempData["FName"];
                            Session["nom"] = f.Nom;
                            return RedirectToAction("../Festivalier/Index", new { username = Session["nom"] });
                        }
                    }
                }

                else
                {
                    festivaliers = Enumerable.Empty<Festivaliers>();

                    ModelState.AddModelError(string.Empty, "Erreur du serveur. Contactez l'administrateur s'il vous plaît.");
                }
                return RedirectToAction("../Festivalier/InscriptionFestivalier");
            }
        }

        public ActionResult Deconnexion()
        {
            Session.Clear();
            return RedirectToAction("../Festival/Index");
        }

    }
}