﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using FestivalSiteWeb.Models;

namespace FestivalSiteWeb.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ICollection<Artiste> artistes = APIControl.Instance.GetArtiste().Result;
            ViewData["artistes"] = artistes;
            return View();
        }

        public ActionResult ConsulterInfosGenerales()
        {
            ICollection<Festival> festivals = APIControl.Instance.GetFest().Result;
            ViewData["festivals"] = festivals;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}